// Farrukh Yuldashboyev, Hometask on creating javascript

function isDivisible (num) {
    /*
    Takes an integer as an argument
    Returns True if the integer is divisble by 5 and 7, else False
    */
    return num%5===0 && num%7===0;
};


function isEvenOrOdd (num) {
    /*
    Takes an integer as an argument
    Returns True if the integer is "Even", else "Odd"
    */
   if (num%2===0) {
    return "Even";
   };
   return "Odd";
};


function SortArray (arr) {
    /*
    Takes an Array as an argument, sorts Array by value
    Returns sorted Array: uses built-in method ( sort() )
    */
    
    return arr.sort(function (a,b) {return a-b});
};


let myObject = [
    {test: ['a', 'b', 'c', 'd']},
    {test: ['a', 'b', 'c']},
    {test: ['a', 'd']},
    {test: ['a', 'b', 'k', 'e', 'e']},
    ]

let uniqueArray = []
function MakeUniqueArray(obj) {
    
    for (item of obj) {
        for (key in item) {
            for (char of item[key]) {
                if (uniqueArray.includes(char) == false) {
                    uniqueArray.push(char);
                }
            }
        }
    }
};




console.log(isDivisible(35)); // --> true
console.log(isEvenOrOdd(24)); // --> Even
console.log(SortArray([4,6,2,6,7,1,661,51,0])); // --> [ 0, 1, 2, 4, 6, 6, 7, 51, 661 ]
MakeUniqueArray(myObject);
console.log(uniqueArray); // --> [ 'a', 'b', 'c', 'd', 'k', 'e' ]
 